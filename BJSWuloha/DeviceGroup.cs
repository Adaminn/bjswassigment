﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BJSWuloha.Devices;

namespace BJSWuloha
{
    internal class DeviceGroup
    {
        public List<Device> Devices { get; set; }
        public string Name { get; set; }
        public DeviceGroup(string name)
        {
            Devices = new List<Device>();
            Name = name;
        }
        public void AddDevice(Device device)
        {
            Devices.Add(device);
        }
    }
}
