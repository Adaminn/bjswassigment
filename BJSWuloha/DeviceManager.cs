﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using BJSWuloha.Devices;
using BJSWuloha.UI;

namespace BJSWuloha
{
    internal class DeviceManager
    {
        //public Dictionary<string, DeviceGroup> DeviceGroups { get; set; }
        public IDictionary<string, DeviceGroup> DeviceGroups { get; set; }
        private int lastUsedId;
        public event EventHandler<DeviceEventArgs>? DeviceEdited;
        public event EventHandler? TreeEdited;
        public DeviceManager()
        {
            lastUsedId = 0;
            DeviceGroups = new Dictionary<string, DeviceGroup>();
        }
        public void AddGroup(DeviceGroup deviceGroup)
        {
            DeviceGroups.Add(deviceGroup.Name, deviceGroup);
            TreeEdited?.Invoke(this, EventArgs.Empty);
        }
        
        public int GenerateNewId()
        {
            return ++lastUsedId;
        }

        public IEnumerable<Device> GetDevices()
        {
            foreach (var group in this.DeviceGroups.Values)
            {
                foreach (var device in group.Devices)
                {
                    yield return device;
                }
            }
        }
        public bool RemoveDeviceByName(string name)
        {
            foreach (var group in this.DeviceGroups.Values)
            {
                foreach (var device in group.Devices)
                {
                    if (device.Name == name)
                    {
                        group.Devices.Remove(device);
                        TreeEdited?.Invoke(this, EventArgs.Empty);
                        return true;
                    }
                }
            }
            return false;
        }
        public bool RemoveGroup(string groupName)
        {
            if (!DeviceGroups.Remove(groupName))
            {
                return false;
            }
            TreeEdited?.Invoke(this, EventArgs.Empty);
            return true;
        }
        public Device? GetDeviceByName(string name)
        {
            foreach (var device in this.GetDevices())
            {
                if (device.Name == name) return device;
            }
            return null;
        }
        public DeviceGroup? GetGroupByName(string name)
        {
            if (!DeviceGroups.TryGetValue(name, out DeviceGroup? group))
            {
                return null;
            }
            return group;
        }
        public bool AddDevice(Device d, string groupName)
        {
            DeviceGroup? group = GetGroupByName(groupName);
            if (group == null) return false;
            group.AddDevice(d);
            TreeEdited?.Invoke(this, EventArgs.Empty);
            return true;
        }
        public bool RenameGroup(string oldName, string newName)
        {
            DeviceGroup? group = GetGroupByName(oldName);
            if (group == null) return false;
            DeviceGroups.Remove(group.Name);
            group.Name = newName;
            AddGroup(group);
            TreeEdited?.Invoke(this, EventArgs.Empty);
            return true;
        }
        public bool MoveDevice(string deviceName, string groupName)
        {
            var device = GetDeviceByName(deviceName);
            if (device == null) { return false; }

            RemoveDeviceByName(device.Name);

            DeviceGroup? group = GetGroupByName(groupName);
            if (group == null) return false;

            group.AddDevice(device);
            TreeEdited?.Invoke(this, EventArgs.Empty);
            return true;
        }
 

        public void Setup1()
        {
            int interval = 500;
            DeviceGroup group1 = new DeviceGroup("main");
            AddGroup(group1);
            Thread.Sleep(interval);
            Speaker s = new Speaker(this.GenerateNewId());
            s.Name = "spikr";
            AddDevice(s, group1.Name);
            Thread.Sleep(interval);
            LedPanel l = new LedPanel(this.GenerateNewId());
            l.Name = "led";
            l.Message = "Original message";
            AddDevice(l, group1.Name);
            Thread.Sleep(interval);
            CardReader c = new CardReader(this.GenerateNewId());
            c.AccessCardNumber = "A01234DE7FFF";
            c.Name = "karta";
            AddDevice(c, group1.Name);
            Thread.Sleep(interval);

            DeviceGroup group2 = new DeviceGroup("custom");
            AddGroup(group2);
            Thread.Sleep(interval);

            Door door = new Door(this.GenerateNewId());
            door.Name = "dvirka";
            door.State = DoorState.Locked;
            AddDevice(door, group2.Name);
            Thread.Sleep(interval);

            RemoveDeviceByName("karta");
            MoveDevice(door.Name, group2.Name);
            Thread.Sleep(interval);
            l.Name = "ice";
            DeviceEdited?.Invoke(this, new DeviceEventArgs(l));
            Thread.Sleep(interval);
            l.Message = "new message";
            DeviceEdited?.Invoke(this, new DeviceEventArgs(l));
            Thread.Sleep(interval);
            door.State = DoorState.Open;
            DeviceEdited?.Invoke(this, new DeviceEventArgs(door));
            Thread.Sleep(interval);
            s.Sound = SpeakerSound.Music;
            DeviceEdited?.Invoke(this, new DeviceEventArgs(s));
            Thread.Sleep(interval);

        }
      
    }
}
