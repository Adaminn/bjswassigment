﻿using BJSWuloha.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BJSWuloha
{

    enum OP
    {
        Invalid = 0,
        AddDevice,
        RemoveDevice,
        EditDevice,
        AddGroup,
        RemoveGroup,
        RenameGroup,
        MoveDevice,
        ShowTree,
        Exit
    }
    internal class IoTSystem
    {
        private OpProcessing process;
        private DeviceManager deviceManager;
        private IOutput output;
        private IInput input;
        public IoTSystem()
        {
            
            deviceManager = new DeviceManager();
            output = new ConsoleOutput();
            input = new ConsoleInput();
            process = new OpProcessing(deviceManager, input, output);
            process.DeviceEdited += OnDeviceEdited;
            deviceManager.DeviceEdited += OnDeviceEdited;
            deviceManager.TreeEdited += OnDeviceManagerEdited;
            deviceManager.Setup1();
        }

        public void OnDeviceManagerEdited(object? sender, EventArgs e)
        {
            output.ShowTree(deviceManager);
        }
        public void OnDeviceEdited(object? sender, DeviceEventArgs e)
        {
            output.ShowChangedDevice(e.Device);

        }
        public void Start()
        {  
          
            output.ShowTree(deviceManager);
           
            while (true)
            {
                try
                {
                    output.ShowOperations();
                    output.ShowEnetrOperation();
                    OP userInput = input.GetOP();
                    switch (userInput)
                    {
                        case OP.Invalid:
                            output.InvalidInput();
                            break;
                        case OP.AddGroup:
                            process.AddGroup();
                            break;
                        case OP.AddDevice:
                            process.AddDevice();
                            break;
                        case OP.RemoveDevice:
                            process.RemoveDevice();
                            break;
                        case OP.RemoveGroup:
                            process.RemoveGroup();
                            break;
                        case OP.RenameGroup:
                            process.RenameGroup();
                            break;
                        case OP.EditDevice:
                            process.EditDevice();
                            break;
                        case OP.MoveDevice:
                            process.MoveDevice();
                            break;
                        case OP.ShowTree:
                            output.ShowTree(deviceManager);
                            break;
                        case OP.Exit:
                            Environment.Exit(0);
                            break;

                        default:
                            output.InvalidInput();
                            break;
                    }
                }
                catch (ArgumentException e)
                {
                    output.ShowErrorMessage(e.Message);
                }
            }       
        }
    }
}
