﻿using BJSWuloha.Devices;
using BJSWuloha.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BJSWuloha
{

    class DeviceEventArgs : EventArgs
    {
        public Device Device { get; set; }
        public DeviceEventArgs(Device device)
        {
            Device = device;
        }
    }
    internal class OpProcessing
    {
        public event EventHandler<DeviceEventArgs>? DeviceEdited;

        readonly DeviceManager deviceManager;
        readonly IInput input;
        readonly IOutput output;
        public OpProcessing(DeviceManager deviceManager, IInput input, IOutput output)
        {
            this.deviceManager = deviceManager;
            this.input = input;
            this.output = output;
        }

        public void RemoveDevice()
        {
            output.EnterDeviceName();
            string name = input.GetDeviceName();
            if (!deviceManager.RemoveDeviceByName(name))
            {
                output.NameNotFound();
            }
            
        }
        public void RemoveGroup()
        {
            output.EnterGroupName();
            string name = input.GetGroupName();
            if (deviceManager.GetGroupByName(name) == null)
            {
                output.NameNotFound();
                return;
            }
            deviceManager.RemoveGroup(name);
        }
        public void AddDevice()
        {
            output.ShowDevicesThatCanBeCreated(deviceManager);
            DeviceType type = input.GetDeviceToCreate(deviceManager);
            Device device;
            switch (type)
            {
                case DeviceType.None:
                    return;
                case DeviceType.LedPanel:
                    device = new LedPanel(deviceManager.GenerateNewId());
                    break;
                case DeviceType.Speaker:
                    device = new Speaker(deviceManager.GenerateNewId());
                    break;
                case DeviceType.Door:
                    device = new Door(deviceManager.GenerateNewId());
                    break;
                case DeviceType.CardReader:
                    device = new CardReader(deviceManager.GenerateNewId());
                    break;
                default:
                    throw new ArgumentException("Device not implemeted yet.");
            }

            FillDeviceParameters(device);

            output.EnterGroupName();
            string name = input.GetGroupName();
            while (deviceManager.GetGroupByName(name) == null)
            {
                output.NameNotFound();
                output.EnterGroupName();
                name = input.GetGroupName();
            }
            deviceManager.AddDevice(device, name);
            
        }

        public void EditDevice()
        {
            output.EnterDeviceName();
            string name = input.GetDeviceName();
            Device? d = deviceManager.GetDeviceByName(name);
            if (d == null)
            {
                output.NameNotFound();
                return;
            }

            FillDeviceParameters(d);

            DeviceEdited?.Invoke(this, new DeviceEventArgs(d));

        }
        public void FillDeviceParameters(Device d)
        {
            string value;
            foreach (var paramInput in d.UIParamSchema)
            {
                output.ShowDeviceParamInput(paramInput);
                value = input.GetDeviceParamInput(paramInput);
                if (value == "") continue;
                ValidateInput(paramInput, value);             
                paramInput.Scope(value);
            }
        }
        private void ValidateInput(DeviceParamInput param, string value) // should be method in the Component class to avoid switch and to encapsulate
        {
            switch (param.Type)
            {
                case DeviceParamType.Name:
                    foreach (var device in deviceManager.GetDevices())
                    {
                        if (device.Name == value)
                        {
                            throw new ArgumentException("Name must be unique");
                        }
                    }
                    break;
            }
        }
        public void AddGroup()
        {
            output.EnterGroupName();
            string name = input.GetGroupName();
            deviceManager.AddGroup(new DeviceGroup(name));
            
        }
        public void RenameGroup()
        {
            // get group
            output.EnterGroupName();
            string currentName = input.GetGroupName();

            while (deviceManager.GetGroupByName(currentName) == null)
            {
                output.NameNotFound();
                output.EnterGroupName();
                currentName = input.GetGroupName();
            }       

            // get new name
            output.EnterGroupName();
            string newName = input.GetGroupName();
            deviceManager.RenameGroup(currentName, newName);
            
            
        }
        public void MoveDevice()
        {
            // get device
            output.EnterDeviceName();
            string name = input.GetDeviceName();
            Device? device = deviceManager.GetDeviceByName(name);
            if (device == null)
            {
                output.NameNotFound();
                return;
            }
   
            // get group
            output.EnterGroupName();
            string groupName = input.GetGroupName();

            while (deviceManager.GetGroupByName(groupName) == null)
            {
                output.NameNotFound();
                output.EnterGroupName();
                groupName = input.GetGroupName();
            }
   
            deviceManager.MoveDevice(device.Name, groupName);
                
        }
    }
}
