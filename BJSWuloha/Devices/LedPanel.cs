﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BJSWuloha.Devices
{
    internal class LedPanel : Device
    {
        public string Message { get; set; }
        public LedPanel(int id) : base(id, DeviceType.LedPanel)
        {
            Message = "";
            UIParamSchema.Add(new UI.DeviceParamInput("Message", DeviceParamType.Text, SetMessage));
        }
        public override string GetCurrnetState()
        {
            return "Message: " + Message;
        }
        public void SetMessage(string message)
        {
            Message = message;
        }
    }
}
