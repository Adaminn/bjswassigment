﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BJSWuloha.Devices
{
    [Flags]
    enum DoorState
    {
        None = 0,
        Locked = 1,
        Open = 2,
        OpenForTooLong = 4,
        OpenedForcibly = 8
    }
    internal class Door : Device
    {
        private DoorState _state;
        public DoorState State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }
        public Door(int id) : base(id, DeviceType.Door)
        {
            UIParamSchema.Add(new UI.DeviceParamInput("Enter Door State", DeviceParamType.DoorCheckBox, SetState));
        }

        public void SetState(string input)
        {
            string[] states = input.Split(' ');
            DoorState result = DoorState.None;
            foreach (var state in states)
            {
                int id;
                if (!int.TryParse(state, out id))
                {
                    throw new ArgumentException();
                }
                if (id >= Enum.GetValues(typeof(DoorState)).Length || id < 0)
                {
                    throw new ArgumentException();
                }
                int exp = (int)Math.Pow(2, id);
                result |= (DoorState)exp;

            }
            State = result;

        }

        public override string GetCurrnetState()
        {
            return "State: " + State;
        }
    }
}
