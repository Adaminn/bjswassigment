﻿using BJSWuloha.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace BJSWuloha.Devices
{
    enum DeviceType
    {
        None,
        LedPanel,
        Door,
        Speaker,
        CardReader
    }

    // should be a class named "Component" and every derived class (Name, DateTime, ...) should have its validation and display method
    enum DeviceParamType  
    {
        None,
        Int,
        Name,
        Text,
        DateTime,
        SpeakerRadioButton,
        DoorCheckBox
    }
    internal abstract class Device
    {

        public int Id { get; }
        public DeviceType Type { get; set; }
        public string Name { get; set; }

        public List<DeviceParamInput> UIParamSchema { get; set; }

        public Device(int id, DeviceType type)
        {
            Id = id;
            Type = type;
            Name = "Unspecified_" + id.ToString();
            UIParamSchema = new List<DeviceParamInput>();
            UIParamSchema.Add(new DeviceParamInput("Name", DeviceParamType.Name, SetName));
        }

        public virtual string GetCurrnetState()
        {
            return "N/A";
        }

        public void SetName(string name)
        {
            Name = name;
        }

    }
}
