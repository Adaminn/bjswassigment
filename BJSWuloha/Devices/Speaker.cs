﻿using BJSWuloha.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BJSWuloha.Devices
{
    enum SpeakerSound
    {
        None,
        Music,
        Alarm
    }
    internal class Speaker : Device
    {
        public float Volume { get; set; }
        public SpeakerSound Sound { get; set; }

        public Speaker(int id) : base(id, DeviceType.Speaker)
        {
            UIParamSchema.Add(new DeviceParamInput("Enter Volume", DeviceParamType.Text, SetVolume));
            UIParamSchema.Add(new DeviceParamInput("Enter Sound Type", DeviceParamType.SpeakerRadioButton, SetSound));
        }

        public override string GetCurrnetState()
        {
            return "Volume: " + Volume +
                ", Sound: " + Sound;
        }
        public void SetVolume(string volume)
        {
            if (int.TryParse(volume, out int vol))
            {
                Volume = vol;
            }
            else
            {
                throw new ArgumentException("Volume must be a number");
            }
        }

        public void SetSound(string sound)
        {
            if (!int.TryParse(sound, out int id))
            {
                throw new ArgumentException();
            }
            if (id >= Enum.GetValues(typeof(SpeakerSound)).Length || id < 0)
            {
                throw new ArgumentException();
            }
            Sound = (SpeakerSound)id;
        }
    }
}
