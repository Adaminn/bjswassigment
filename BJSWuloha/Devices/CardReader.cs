﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BJSWuloha.Devices
{
    internal class CardReader : Device
    {
        private string _cardNumber = "N/A";
        public string AccessCardNumber
        {
            get
            {
                return _cardNumber;
            }
            set
            {
                if (HasEvenCountOfChars(value) && IsHex(value) && value.Length <= 16)
                {
                    _cardNumber = ReverseBytesAndPad(value);
                }
                else
                {
                    throw new ArgumentException("Carn number must be even, hex and greater than 16");
                }
            }
        }
        public CardReader(int id) : base(id, DeviceType.CardReader)
        {
            UIParamSchema.Add(new UI.DeviceParamInput("Enter Card Number", DeviceParamType.Text, CardNumberSetter));
        }
        public override string GetCurrnetState()
        {
            return "Access Card Number: " + AccessCardNumber;
        }
        private bool HasEvenCountOfChars(string text)
        {
            return text.Length % 2 == 0;
        }
        private bool IsHex(string text)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(text, @"\A\b[0-9a-fA-F]+\b\Z");
        }
        private string ReverseBytesAndPad(string text)
        {
            string res = AppendZeroes(text);
            res = ReverseByTwo(res);
            return res;
        }
        private string AppendZeroes(string text)
        {
            string res = text;
            for (int i = 0; i < 16 - text.Length; i++)
            {
                res += '0';
            }

            return res;
        }
        private string ReverseByTwo(string text)
        {
            string res = "";
            for (int i = text.Length - 1; i > 0; i -= 2)
            {

                res += text[i - 1];
                res += text[i];
            }
            return res;
        }

        public void CardNumberSetter(string number)
        {
            AccessCardNumber = number;
        }
    }
}
