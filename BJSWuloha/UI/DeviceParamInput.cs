﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BJSWuloha.Devices;

namespace BJSWuloha.UI
{
    internal class DeviceParamInput
    {
        public string Label { get; set; }
        public DeviceParamType Type { get; set; }
        public Action<string> Scope { get; set; }

        public DeviceParamInput(string label, DeviceParamType type, Action<string> scope)
        {
            Label = label;
            Type = type;
            Scope = scope;
        }
    }
}
