﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BJSWuloha.Devices;

namespace BJSWuloha.UI
{
    internal class ConsoleOutput : IOutput
    {
        public void ShowTree(DeviceManager dm)
        {
            if (dm.DeviceGroups.Count == 0)
            {
                Console.WriteLine("No devices found");
                return;
            }

            const string EDGE = "│ ";
            const string NODE = "├─";
            Console.WriteLine(".");
            foreach (var group in dm.DeviceGroups.Values)
            {
                Console.WriteLine(NODE + group.Name);

                foreach (var device in group.Devices)
                {
                    Console.WriteLine(EDGE + NODE + device.Name + "  | "+ device.GetCurrnetState());
                }
            }
            Console.WriteLine("");

        }

        public void ShowOperations()
        {
            int i = 0;
            foreach (OP op in Enum.GetValues(typeof(OP)))
            {

                if (op == OP.Invalid) { continue; }
                i++;
                Console.WriteLine(i + ". " + op);
           
            }
        }
        public void ShowEnetrOperation()
        {
            Console.WriteLine("Enter a number corresponding to an action you want to run:");
        }
        public void InvalidInput()
        {
            Console.WriteLine("Invalid input.");
        }
        public void NameNotFound()
        {
            Console.WriteLine("Name not found.");
        }
        public void EnterGroupName()
        {
            Console.WriteLine("Enter group name:");
        }
        public void EnterDeviceName()
        {
            Console.WriteLine("Enter device name:");
        }
        public void ShowChangedDevice(Device d)
        {
            Console.WriteLine(String.Format("Device \"{0}\" changed:", d.Name));
            ShowDevice(d);
        }
        public void ShowDevice(Device d)
        {
            Console.WriteLine(d.GetCurrnetState());
        }
        public void ShowErrorMessage(string message)
        {
            Console.WriteLine(message);
        }
        public void ShowDevicesThatCanBeCreated(DeviceManager dm)
        {
            int i = 0;
            foreach (DeviceType device in Enum.GetValues(typeof(DeviceType)))
            {        
                Console.WriteLine(i + ". " + device);
                i++;
            }
        }
        public void ShowDeviceParamInput(DeviceParamInput param) // should be methond in the Component class to avoid switch and to encapsulate
        {
            Console.WriteLine(param.Label + " (Enter to skip): ");
            switch (param.Type) 
            {
                case DeviceParamType.DoorCheckBox:
                    Console.Write(DoorCheckBox());
                    break;
                case DeviceParamType.SpeakerRadioButton:
                    Console.Write(SpeakerRadioButton());
                    break;
                default:
                    break;
            }
        }
        private string DoorCheckBox()
        {
            string result = "";
            int i = 0;
            foreach (DoorState state in Enum.GetValues(typeof(DoorState)))
            {
                if (state == DoorState.None) { continue; }
                result += "Type " + i + " for " + state + "\n";
                i++;
            }
            result += "Type multiple values seperated by space\n";
            return result;
        }
        private string SpeakerRadioButton()
        {
            string result = "";
            int i = 0;
            foreach (SpeakerSound state in Enum.GetValues(typeof(SpeakerSound)))
            {
                result += "Type " + i + " for " + state + "\n";
                i++;
            }
            return result;
        }



    }
}
