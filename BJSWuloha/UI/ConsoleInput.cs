﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BJSWuloha.Devices;

namespace BJSWuloha.UI
{
    internal class ConsoleInput : IInput
    {
        public OP GetOP()
        {
            string? input = Console.ReadLine();
            if (input == null)
            {
                return OP.Invalid;
            }

            if (!int.TryParse(input, out int id))
            {
                return OP.Invalid;
            }
            if (id >= Enum.GetValues(typeof(OP)).Length || id < 0)
            {
                return OP.Invalid;
            }
            return (OP)id;

        }
        public string GetDeviceName()
        {
            return GetGroupName();
        }
        public string GetGroupName()
        {
            string? input = Console.ReadLine();
            if (input == null)
            {
                return "";
            }
            return input;
        }
        public DeviceType GetDeviceToCreate(DeviceManager dm)
        {
            string? input = Console.ReadLine();
            if (input == null)
            {
                throw new ArgumentException();
            }
            if (!int.TryParse(input, out int id))
            {
                throw new ArgumentException();
            }
            if (id >= Enum.GetValues(typeof(DeviceType)).Length || id < 0)
            {
                throw new ArgumentException();
            }
            return (DeviceType)id;
        }

        public string GetDeviceParamInput(DeviceParamInput param)
        {

            string? input = Console.ReadLine();
            if (input == null)
            {
                throw new ArgumentException();
            }
            return input;

        }
    }
}
