﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BJSWuloha.Devices;

namespace BJSWuloha.UI
{
    internal interface IOutput
    {
        public void ShowErrorMessage(string msg);
        public void ShowTree(DeviceManager dm);
        public void ShowOperations();
        public void ShowDevice(Device d);
        public void ShowChangedDevice(Device d);

        public void ShowEnetrOperation();
        public void InvalidInput();
        public void EnterDeviceName();
        public void EnterGroupName(); 
        public void NameNotFound();
        public void ShowDeviceParamInput(DeviceParamInput d);

        public void ShowDevicesThatCanBeCreated(DeviceManager dm);


    }
}
