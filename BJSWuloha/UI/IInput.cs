﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BJSWuloha.Devices;

namespace BJSWuloha.UI
{
    internal interface IInput
    {
        public OP GetOP();
        public string GetDeviceName();
        public string GetGroupName();
        public DeviceType GetDeviceToCreate(DeviceManager dm);
        public string GetDeviceParamInput(DeviceParamInput d);
    }
}
